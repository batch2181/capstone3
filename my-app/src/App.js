import "./App.css"
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'
import { Container } from 'react-bootstrap';


import Home from "./pages/Home";
import Login from './pages/Login'
import AppNavbar from "./components/AppNavbar";
import Register from "./pages/Register";
import Admin from "./pages/Admin";
import Products from "./pages/Products";


function App() {


  return(
    <Router>
      <AppNavbar/>
      <Container>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/products' element={<Products/>}/>
        <Route path='/admin' element={<Admin/>}/>
      </Routes>
      </Container>
    </Router>
  )
}

export default App