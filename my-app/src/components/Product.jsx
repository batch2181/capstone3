import React from "react";

export default function Product(props) {
  return (
    <div className="card">
      <img className="product--image" src={props.url} alt="product image" />
      <p>
        <button className="bg-sky-700 font-light">ADD TO CART</button>
      </p>
      <h2 className="bg-sky-100 cursor-pointer font-light text-[14px]">{props.name}</h2>
      <p className="bg-sky-100 cursor-pointer font-light font-poppins text-[14px]">{props.price}</p>
      <br/>
    </div>
  )}