import "../App.css";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import Product from "./Product";
import { productData, responsive } from "../data/data";
import style from "../style"

export default function Featured() {
  const product = productData.map((item) => (
    <Product
      name={item.name}
      url={item.imageurl}
      price={item.price}
      description={item.description}
    />
  ));

  return (
    <div className={`${style.myFeatured} dark:bg-gray-900 overflow-hidden`}>
      <div className="App font-poppins group">
        <Carousel responsive={responsive}>
          {product}
        </Carousel>
      </div>
    </div>
  )
}