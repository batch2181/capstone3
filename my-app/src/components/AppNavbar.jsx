import React from 'react'
import logo from '../assets/logo.png'
import { Link, useMatch, useResolvedPath } from "react-router-dom"

function AppNavbar() {


  return (
    <body>
      <nav className='dark:bg-gray-900 flex justify-between items-center'>
        <a href='/' className="pl-[55px] pt-[12px] py-3">
          <img className="w-[90px]" src={logo}/>
        </a>
        <div className='navbar-links px-[64px] py-5'>
          <ul className='text-white flex gap-10 font-extralight'>
            <CustomLink to="/">Home</CustomLink>
            <CustomLink to="/products">Products</CustomLink>
            <CustomLink to="/login">Login</CustomLink>
            <CustomLink to="/register">Register</CustomLink>
          </ul>
        </div>  
      </nav>
    </body>  

  )
};

function CustomLink({ to, children, ...props }) {
  const resolvedPath = useResolvedPath(to)
  const isActive = useMatch({ path: resolvedPath.pathname, end: true })

  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
  )
}

export default AppNavbar