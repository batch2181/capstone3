import React from 'react'

export default function Footer() {
  return (
    <div className='bg-primary h-[200px]'>
        <div className='text-white text-center'>
            FOOTER
        </div>
    </div>
  )
}
