const styles = {
    myNavbar : "xl:max-w-[1800px] w-full",
    myHome : "xl:max-w-[1800px] w-full",
    myFeatured : "px-[48px] py-[20px]",
}

export default styles