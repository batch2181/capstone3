

export default function Login() {


    return(
        <div className="dark:bg-gray-900 h-screen pt-[80px]">
            <div className="text-center flex items-center justify-center">
                <form className="bg-white h-[400px] w-[350px] rounded-[15px] px-7 pt-7">
                    <h1 className="text-dark text-[23px] font-extralight">Login</h1>
                    <div className='flex flex-row items-center justify-center items-center py-[6px] px-4 bg-discount-gradient rounded-[10px] mb-2'>
                        <input className="rounded-[3px] text-center flex text-[14px] font-light mt-[25px] border-b-2 w-[100%]" type='text' placeholder='Enter your email...' required/>
                    </div>
                    <label className="text-dark font-extralight text-[16px]">Email</label>
                    <div className='flex flex-row items-center py-[6px] px-4 bg-discount-gradient rounded-[10px] mb-2 justify-center items-center'>
                        <input className="rounded-[3px] text-center flex text-[14px] font-light mt-[25px] border-b-2 w-[100%]" type='text' placeholder='Enter your password...' required/>
                    </div>
                    <label className="text-dark font-extralight text-[16px]">Password</label>
                    <div className="darl:bg-gray-900 text-white p-3 pr-5 pl-5">
                        <button variant="primary" type="submit" id="submitBtn " className="text-gray-200 bg-sky-700 mt-4 w-[100px] text-[14px] font-light rounded-[5px] p-1">Login</button>
                    </div>
                    <div class="signup_link" className="text-[12px] font-light p-2 mt-3">
                    Not a member? <a href="#">Signup</a>
                    </div>
                </form>
            </div>
        </div>
    )
}