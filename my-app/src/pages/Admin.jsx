import React, { useEffect } from 'react'
import {useState} from 'react'
// import Axios from 'axios'

function Admin() {

const [listOfUsers, setListOfUsers] = useState([]);

useEffect(() => {
    fetch(`http://localhost:3000/users/`)
    .then(res => res.json())
    .then(data => {

      setListOfUsers(data.map(user => {
        return (
            <div key={user._id}/>
        )
      }));
    });
  }, []);


  return (
        <div className='userDisplay'>
            <div>
                <h1>Users{listOfUsers} </h1>
            </div>
        </div>
  )
}

export default Admin