import Carousel from "../components/Carousel";
import Featured from "../components/Featured";
import style from "../style"


export default function Home() {


return ( 
    <div className="dark:bg-gray-900">
        <div className={`${style.myHome} grid grid-cols-10 px-[64px] py-[20px]`}>
            <div className="col-start-1 col-span-4 font-poppins">
                <h2 className="font-bold text-white text-[72px] leading-[80px]">
                Shop for the 
                </h2>
                <h2 className="font-bold text-gradient text-[72px] leading-[80px]">
                Best Gaming
                </h2>
                <h2 className="font-bold text-white text-[72px] leading-[80px]">
                Experience.
                </h2>
                <p className="mt-5 text-white font-light">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas aliquet pretium eros euismod pretium.
                </p>
                <button className="bg-blue-gradient font-medium p-4 h-[30px] flex items-center mt-[40px] text-[13px] rounded-[3px]">
                SHOP NOW
                </button>
            </div>
            <Carousel/>
        </div>
        <div>
            <Featured/>
        </div>
    </div>
  );
}
