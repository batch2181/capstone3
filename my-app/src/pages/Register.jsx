import React from 'react'
import { Form, Button } from 'react-bootstrap';
import { useState } from 'react';

function Register() {

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");



return (
    <div className='dark:bg-gray-900 h-screen'>
        <div>
        </div>
        <form className='relative top-[40px] bg-white h-[550px] w-[450px] mx-auto rounded-[10px] px-[50px] py-[20px] font-poppins items-center'>
            <h1 className="flex text-center items-center justify-center mb-[30px] pt-[15px] font-extralight text-[18px]">REGISTER</h1>
            <Form.Group className="mb-2 text-[16px] font-light" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <div>
                        <input
                            type="text"
                            value={firstName}
                            onChange={(e) => {setFirstName(e.target.value)}}
                            required
                            className='border-b-[2px] w-full text-[14px] opacity-75 '
                            placeholder='Enter your first name...'
                            />
                </div>
            </Form.Group>
            <Form.Group className="mb-2 text-[16px] font-light" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <div>
                    <input
                        type="text"
                        value={lastName}
                        onChange={(e) => {setLastName(e.target.value)}}
                        className='border-b-[2px] w-full text-[14px] opacity-75'
                        placeholder='Enter your last name...'
                        />
                </div>
            </Form.Group>

            <Form.Group className="mb-2 text-[16px] font-light" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <div>
                    <input
                        type="email"
                        value={email}
                        onChange={(e) => {setEmail(e.target.value)}}
                        className='border-b-[2px] w-full text-[14px] opacity-75'
                            placeholder='Enter your email...'
                        />
                </div>
            </Form.Group>

            <Form.Group className="mb-2 text-[16px] font-light" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <div>
                    <input
                        type="text"
                        value={mobileNo}
                        onChange={(e) => {setMobileNo(e.target.value)}}
                        className='border-b-[2px] w-full text-[14px] opacity-75'
                            placeholder='Enter your mobile number...'
                        />
                </div>
            </Form.Group>

            <Form.Group className="mb-2 text-[16px] font-light" controlId="password1">
                <Form.Label>Password</Form.Label>
                <div>
                    <input
                        type="password" 
                        value={password1}
                        onChange={(e) => {setPassword1(e.target.value)}}
                        className='border-b-[2px] w-full text-[14px] opacity-75'
                            placeholder='Enter your password...'
                        />
                </div>
            </Form.Group>

            <Form.Group className="mb-6 text-[16px] font-light" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <div>
                    <input
                    type="password" 
                    value={password2}
                    onChange={(e) => {setPassword2(e.target.value)}}
                    className='border-b-[2px] w-full text-[14px] opacity-75'
                            placeholder='Verify your password...'
                    />
                </div>
            </Form.Group>
            <div className='flex justify-center items-center'>
                <Button className='text-center font-extralight text-gray-200 bg-sky-700 px-[40px] py-[5px] rounded-[5px]' variant="primary" type="submit" id="submitBtn" disabled>
                        Submit
                </Button>
            </div>
            <div class="signup_link" className="text-[14px] font-light p-2 mt-3 flex justify-center items-center "> Already have an account? <a className='ml-1' href="#">Login Here</a>
            </div>
        </form>	
    </div>
    )
}

export default Register